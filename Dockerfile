FROM nicopaez/jobvacancy-dev:2.5.7
RUN apt-get update && apt-get install -yq \
    firefox-esr
RUN wget -q "https://github.com/mozilla/geckodriver/releases/download/v0.28.0/geckodriver-v0.28.0-linux64.tar.gz" -O /tmp/geckodriver.tgz \
    && tar zxf /tmp/geckodriver.tgz -C /usr/bin/ \
    && rm /tmp/geckodriver.tgz
