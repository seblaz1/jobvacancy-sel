Feature: Job Offers expiration date
  In order to not receive job applications past a certain date
  As a job offerer
  I want to specify an expiration date to a job offer

  Background:
    Given I am logged in as job offerer

  Scenario Outline: Create new offer with expiration date
    Given today is "27/11/2020"
    When I create a new offer with "Developer" as the title and "<date>" as expiration date
    Then I should see the title "Developer" and the expiration date "<date>" in my offers list

    Examples:
      |    date    |
      | 01/12/2020 |
      | 01/01/2021 |
      | 27/11/2020 |

  Scenario Outline: Create new offer with invalid expiration date
    Given today is "27/11/2020"
    When I create a new offer with "Developer" as the title and "<date>" as expiration date
    Then I should see an error message "can't be in the past"

    Examples:
      |    date    |
      | 01/11/2020 |
      | 26/11/2020 |

  Scenario: Update offer with expiration date
    Given I have "Developer" offer in my offers list
    When I edit the job offer
    Then I should not be able to edit the expiration date
