require_relative 'actions'

When('I register successfully completing the captcha') do
  register_user_with(catpcha: true)
end

When('I register without completing the captcha') do
  register_user_with(catpcha: false)
end
