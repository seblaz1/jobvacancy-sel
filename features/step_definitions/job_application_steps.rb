Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  @job_offer = JobOffer.create({
                                 title: job_title,
                                 location: 'a nice job',
                                 description: 'a nice job'
                               }, JobVacancy::App.settings.clock)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given(/^I access the offers list page$/) do
  visit '/job_offers'
end

When(/^I apply$/) do
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
  click_button('Apply')
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Given('only a {string} offer with expiration date in {string} exists in the offers list') do |title, expiration_date|
  expiration_date = Time.parse(expiration_date)
  post_date = expiration_date - 24 * 60 * 60
  visit "/health/time?time=#{post_date}"
  @job_offer = JobOffer.create({
                                 title: title,
                                 location: 'a nice job',
                                 description: 'a nice job',
                                 expiration_date: Time.new(expiration_date.year,
                                                           expiration_date.month,
                                                           expiration_date.day,
                                                           23, 59, 59)
                               }, JobVacancy::App.settings.clock)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Then('I should see {string} in the offers list') do |title|
  visit '/job_offers'
  page.should have_content(title)
end

Then('I should not see {string} in the offers list') do |title|
  visit '/job_offers'
  page.should have_no_content(title)
end
