require_relative 'actions'

Given('a job offerer with email {string} already exists') do |email|
  register_user_with(email: email)
end

When('I register with email {string}') do |email|
  register_user_with(email: email)
end

Then('I should see a succesful registration message') do
  page.should have_content('User created')
end
