require_relative 'actions'

Given('I am not Registered') do
  visit '/'
end

When('I register with email {string} and password {string}') do |email, password|
  register_user_with(email: email, password: password)
end

Then('I should be able to login with email {string} and that password') do |_email|
  page.should have_content('User created')
end

Then('I should see a message {string}') do |message|
  page.should have_content(message)
end
