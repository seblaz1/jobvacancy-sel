When('I apply with the email {string}') do |email|
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: email)
  click_button('Apply')
end

Then('I should receive a contact confirmation message') do
  page.should have_content('Contact information sent.')
end

Then('I should see an error message {string}') do |message|
  page.should have_content(message)
end
