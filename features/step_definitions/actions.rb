def register_user_with(name: 'Juan', email: 'of@test.com', password: 'p@assword', catpcha: true)
  visit '/register'
  fill_in('user[name]', with: name)
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)

  if catpcha
    frame = find('.g-recaptcha iframe')
    page.within_frame(frame) do
      checkbox = find('.recaptcha-checkbox')
      checkbox.click
      should have_css('.recaptcha-checkbox-checked')
    end
  end

  click_button('Create')
end
