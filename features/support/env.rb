require File.expand_path(File.dirname(__FILE__) + '/../../config/boot')

require 'capybara/cucumber'
require 'rspec/expectations'
require 'selenium-webdriver'

require 'simplecov'

SimpleCov.start do
  root(File.join(File.dirname(__FILE__), '..', '..'))
  coverage_dir 'reports/coverage'
  add_filter '/spec/'
  add_filter '/features/'
  add_filter '/admin/'
  add_filter '/db/'
  add_filter '/config/'
  add_group 'Models', 'app/models'
  add_group 'Controllers', 'app/controllers'
  add_group 'Helpers', 'app/helpers'
end

user_repository = UserRepository.new
unless user_repository.all.count.positive?
  user = User.new(
    email: 'offerer@test.com',
    name: 'Offerer',
    password: 'Passw0rd!'
  )

  user_repository.save(user)
end

Around do |_scenario, block|
  DB.transaction(rollback: :always, auto_savepoint: true) { block.call }
end

# Capybara.default_driver = :selenium
Capybara.app = JobVacancy::App.tap { |app| }

Capybara.register_driver :remote_driver do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.firefox
  capabilities['browserName'] = 'firefox'
  capabilities['javascriptEnabled'] = true
  Capybara::Selenium::Driver.new(app,
                                 browser: :remote,
                                 desired_capabilities: capabilities,
                                 url: 'http://selenium:4444/wd/hub')
end

Capybara.javascript_driver = :remote_driver
Capybara.server_port = Selenium::WebDriver::PortProber.above(3000)
Capybara.server_host = '0.0.0.0'
Capybara.app_host = "http://#{`hostname -s`.strip}:#{Capybara.server_port}"
