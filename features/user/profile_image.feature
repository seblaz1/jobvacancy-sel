Feature: Profile Image
  In order to see my face in the screen
  As a user
  I want to only be able to set my profile image

  @wip
  Scenario: Profile image with an email registered in Gravatar
    Given I am logged in as job offerer
    When I browse the default page
    Then I should see my profile image

  @wip
  Scenario: Profile image without an email registered in Gravatar
    Given I am logged in as job offerer
    When I browse the default page
    Then I should see an avatar image

