Feature: Job Application
  In order to see valid offers
  As a candidate
  I want to only see unexpired offers

  Background:
    Given only a "Ruby Programmer" offer with expiration date in "27/11/2020" exists in the offers list

  Scenario: Job offer not expired
    Given today is "26/11/2020"
    When I access the offers list page
    Then I should see "Ruby Programmer" in the offers list
    And I should see "27/11/2020" in the offers list

  Scenario: Job offer not expired same day
    Given today is "27/11/2020"
    When I access the offers list page
    Then I should see "Ruby Programmer" in the offers list
    And I should see "27/11/2020" in the offers list

  Scenario: Job offer already expired
    Given today is "28/11/2020"
    When I access the offers list page
    Then I should not see "Ruby Programmer" in the offers list
