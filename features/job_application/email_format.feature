Feature: Job Application email format
  In order to get a job
  As a candidate
  I want to only be able to apply with a valid email

  Background:
    Given only a "Web Programmer" offer exists in the offers list


  Scenario Outline: Valid email to apply to offer
    Given I access the offers list page
    When I apply with the email <mail>
    Then I should receive a contact confirmation message

    Examples:
      | mail                 |
      | "test@fi.uba.ar"     |
      | "test@gmail.com"     |
      | "test70@test.com"    |
      | "test_70@test.com"   |
      | "test-Mail@test.com" |

  Scenario Outline: Invalid email to apply to offer
    Given I access the offers list page
    When I apply with the email <mail>
    Then I should see an error message "Invalid email format to apply."

    Examples:
      | mail                |
      | "te@st@fi.uba.ar"   |
      | "test@gmail..com"   |
      | "te..st70@test.com" |
      | "test_70test.com"   |
      | "            "      |