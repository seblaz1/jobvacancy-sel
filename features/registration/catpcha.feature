Feature: Captcha
  In order to improve the system security
  As the application owner
  I want users to complete a captcha when they register

  @javascript
  Scenario: Captcha completed
    When I register successfully completing the captcha
    Then I should see a message "User created"

  @javascript
  Scenario: Captcha not completed
    When I register without completing the captcha
    Then I should see a message "Captcha required"
