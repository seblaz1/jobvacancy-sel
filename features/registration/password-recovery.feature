Feature: Password recovery
  In order to recover access to my account
  As a job offerer
  I want to recover my password

  @wip
  Scenario: Recover password
    Given I am registered with email "of@test.com"
    When I try to recover my password
    Then I should be receive an email with a password recovery link

  @wip
  Scenario: Not existent user
    Given I am not registered with email "of@test.com"
    When I try to recover my password
    Then I should not recieve an email with a password recovery link

  @wip
  Scenario: Recover password
    Given I am registered with email "of@test.com"
    When I try to recover my password
    And I change my password to "p@ssword"
    Then I should be able to login with email "of@test.com" and password "p@ssword"

