Feature: Strong password
  In order to improve my account's security
  As a job offerer
  I want to only be able to register with a strong password

  @javascript
  Scenario Outline: Valid Password
    Given I am not Registered
    When I register with email "of@test.com" and password <password>
    Then I should be able to login with email "of@test.com" and that password
    Examples:
      | password   |
      | "Passw@rd1234" |
      | "Passw!rd1234" |
      | "Passw$rd1234" |
      | "Passw?rd1234" |
      | "Passw-rd1234" |
      | "Passw\rd1234" |


  @javascript
  Scenario: Password Too Short
    Given I am not Registered
    When I register with email "of@test.com" and password "Pass@"
    Then I should see a message "it must be between 8 and 32 characters"

  @javascript
  Scenario: Password Too Long
    Given I am not Registered
    When I register with email "of@test.com" and password "Pass@wordMetodosYModelosDeIngSoft2"
    Then I should see a message "it must be between 8 and 32 characters"


  @javascript
  Scenario: Password weak
    Given I am not Registered
    When I register with email "of@test.com" and password "Password"
    Then I should see a message "one special character (not alphanumeric)"
