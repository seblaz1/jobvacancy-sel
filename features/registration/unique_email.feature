Feature: Unique email
  In order to ensure my identity
  As a job offerer
  I want to only be able to register with a unique email

  Background:
    Given a job offerer with email "jobofferer@test.com" already exists

  @javascript
  Scenario: Unique email
    When I register with email "of@test.com"
    Then I should see a succesful registration message

  @javascript
  Scenario: Email already exists
    When I register with email "jobofferer@test.com"
    Then I should see a message "User email already registered"

  @wip
  Scenario Outline: Email already exists
    When I register with email "<email>"
    Then I should see a message "User email already registered"

    Examples:
      | email                     |
      | jobofferer@test.com       |
      | jobofferer+memo2@test.com |
      | job.offerer@test.com      |
      | job.offerer+memo@test.com |
