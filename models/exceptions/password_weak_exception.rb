class PasswordInvalidException < RuntimeError
  def self.too_long(max_length)
    new("Password too long. Maximum #{max_length} characters")
  end
end

class PasswordWeakException < PasswordInvalidException
  def self.too_short(min_length)
    new("Password too short. Minimum #{min_length} characters")
  end

  def self.without_special_characters
    new('Password weak. Minimum 1 special character (not alphanumeric)')
  end
end
