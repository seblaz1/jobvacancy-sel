class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title,
                :location, :description, :is_active, :expiration_date,
                :updated_on, :created_on

  validates :title, presence: true
  validate :expiration_date_cannot_be_in_the_past

  def self.create(data, clock)
    job_offer = new(data, clock)
    job_offer.validate!
    job_offer
  end

  def initialize(data, clock)
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    @expiration_date = data[:expiration_date]
    @clock = clock
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  private

  def expiration_date_cannot_be_in_the_past
    if expiration_date.present? && expiration_date < @clock.now
      errors.add(:expiration_date, "can't be in the past")
    end
  end
end
