class User
  include ActiveModel::Validations

  attr_accessor :id, :name, :email, :crypted_password, :job_offers, :updated_on, :created_on

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  VALID_PASSWORD_REGEX = /(?=.*[\W_])([\w\W\S]|[^ ])/im
  PASSWORD_MIN_LENGTH = 8
  PASSWORD_MAX_LENGTH = 32

  validates :name, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                              message: 'invalid email' }
  validates :crypted_password, presence: true

  def initialize(data = {}, user_repository = nil)
    unique_email_validation(data[:email], user_repository)

    @id = data[:id]
    @name = data[:name]
    @email = data[:email]
    @crypted_password = crypted(data[:password], data[:crypted_password])
    @job_offers = data[:job_offers]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
  end

  def unique_email_validation(email, user_repository)
    unless user_repository.nil?
      raise DuplicateUserEmailException unless user_repository.find_by_email(email).nil?
    end
  end

  def has_password?(password)
    Crypto.decrypt(crypted_password) == password
  end

  private

  def crypted(password, crypted_password)
    return crypted_password if password.nil?

    Crypto.encrypt(password) if strong_password?(password)
  end

  def strong_password?(password)
    raise PasswordWeakException.too_short(PASSWORD_MIN_LENGTH) if password_too_short?(password)
    raise PasswordInvalidException.too_long(PASSWORD_MAX_LENGTH) if password_too_long?(password)
    raise PasswordWeakException.without_special_characters unless
      password_has_special_character?(password)

    true
  end

  def password_too_short?(password)
    password.length < PASSWORD_MIN_LENGTH
  end

  def password_too_long?(password)
    password.length > PASSWORD_MAX_LENGTH
  end

  def password_has_special_character?(password)
    password.match?(VALID_PASSWORD_REGEX)
  end
end
