class JobApplication
  include ActiveModel::Validations

  attr_accessor :applicant_email
  attr_accessor :job_offer

  VALID_EMAIL_REGEX = /\A(?=^.*@?)(?!.*\.\..*)([\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+$)\z/i

  validates :job_offer, presence: true
  validates :applicant_email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                                        message: 'invalid format' }

  def initialize(email, offer)
    @applicant_email = email
    @job_offer = offer
    validate!
  end

  def self.create_for(email, offer)
    JobApplication.new(email, offer)
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end
end
