class Clock
  def initialize(time = nil)
    @now = -> { time || Time.new }
  end

  def now
    @now.call
  end

  def now=(time)
    @now = -> { time }
  end
end
