Sequel.migration do
  up do
    add_column :job_offers, :expiration_date, Time
  end

  down do
    drop_column :job_offers, :expiration_date
  end
end
