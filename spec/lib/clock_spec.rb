require 'spec_helper'

describe 'clock' do
  it 'when I ask for the current time then I get approximately the current time' do
    clock = Clock.new
    expect(clock.now).to be_within(1).of Time.now
  end

  it 'when I set the current time and I ask for the current time then I get exactly the same time' do
    clock = Clock.new
    time = Time.new(2020, 12, 5, 19, 15)
    clock.now = time
    expect(clock.now).to eql(time)
  end

  it 'when I initialize the clock with a given time and I ask for the current time then I get exactly the same time' do
    time = Time.new(2020, 12, 5, 19, 15)
    clock = Clock.new(time)
    expect(clock.now).to eql(time)
  end
end
