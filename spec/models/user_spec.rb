require 'spec_helper'
require './models/exceptions/password_weak_exception'

describe User do
  subject(:user) { described_class.new({}) }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:crypted_password) }
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:job_offers) }
  end

  describe 'valid?' do
    it 'should be false when name is blank' do
      user = described_class.new(email: 'john.doe@someplace.com',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:name)
    end

    it 'should be false when email is not valid' do
      user = described_class.new(name: 'John Doe', email: 'john',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:email)
    end

    it 'should be false when password is blank' do
      user = described_class.new(name: 'John Doe', email: 'john')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:crypted_password)
    end

    it 'should be true when all field are valid' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq true
    end
  end

  describe 'has password?' do
    let(:password) { 'password@' }
    let(:user) do
      described_class.new(password: password,
                          email: 'john.doe@someplace.com',
                          name: 'john doe')
    end

    it 'should return false when password do not match' do
      expect(user).not_to have_password('invalid')
    end

    it 'should return true when password do  match' do
      expect(user).to have_password(password)
    end
  end

  describe 'strongs passwords' do
    it 'Pass@ should be a short password' do
      password = 'Pass@'
      expect do
        described_class.new(password: password, email: 'john.doe@someplace.com',
                            name: 'john doe')
      end      .to raise_error(PasswordWeakException)
    end

    it 'Password@123456789012345678901234 should be a long password' do
      password = 'Password@123456789012345678901234'
      expect do
        described_class.new(password: password, email: 'john.doe@someplace.com',
                            name: 'john doe')
      end      .to raise_error(PasswordInvalidException)
    end

    it 'should be invalid if not contains a special character' do
      password = 'Password67890'
      expect do
        described_class.new(password: password, email: 'john.doe@someplace.com',
                            name: 'john doe')
      end .to raise_error(PasswordWeakException)
    end
  end

  describe 'unique user email' do
    let(:user) do
      described_class.new(name: 'John Doe', email: 'john@test.com',
                          password: 'a_secure_passWord!')
    end
    let(:user_repository) { double }

    it 'should raise DuplicateUserEmailException when register the same email twice' do
      allow(user_repository).to receive(:find_by_email).and_return(user)

      expect do
        described_class.new({ name: 'John Doe', email: 'john@test.com',
                              password: 'a_secure_passWord!' }, user_repository)
      end .to raise_error(DuplicateUserEmailException)
    end

    it 'should be able to create a new user with a different email address' do
      allow(user_repository).to receive(:find_by_email).and_return(nil)

      expect do
        described_class.new({ name: 'John Doe', email: 'john@test.com',
                              password: 'a_secure_passWord!' }, user_repository)
      end .not_to raise_error
    end
  end
end
