require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.create({ location: 'a location' }, Clock.new)
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.create({ title: 'a title' }, Clock.new)
      expect(job_offer).to be_valid
    end

    it 'should be invalid when expiration date is yesterday' do
      today = Time.new(2020, 12, 10, 23, 59, 0)
      yesterday = today - 24 * 60 * 60 # -1 day
      clock = Clock.new(today)

      check_validation(:expiration_date, "Expiration date can't be in the past") do
        described_class.create({ title: 'a title', expiration_date: yesterday }, clock)
      end
    end

    it 'should be valid when expiration date is today' do
      today = Time.new(2020, 12, 10, 23, 59, 0)
      clock = Clock.new(today)
      job_offer = described_class.create({ title: 'a title', expiration_date: today }, clock)
      expect(job_offer).to be_valid
    end
  end
end
