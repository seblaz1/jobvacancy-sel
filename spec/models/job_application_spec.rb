require 'spec_helper'

describe JobApplication do
  let(:job_offer) { JobOffer.create({ title: 'a title' }, Clock.new) }

  describe 'valid?' do
    it 'should be invalid when email is blank' do
      check_validation(:applicant_email, "Applicant email can't be blank") do
        described_class.create_for(nil, job_offer)
      end
    end

    it 'should be invalid when offer is blank' do
      check_validation(:job_offer, "Job offer can't be blank") do
        described_class.create_for('applicant@test.com', nil)
      end
    end
  end

  describe 'create_for' do
    it 'should set applicant_email' do
      email = 'applicant@test.com'
      ja = described_class.create_for(email, job_offer)
      expect(ja.applicant_email).to eq(email)
    end

    it 'should set job_offer' do
      offer = job_offer
      ja = described_class.create_for('applicant@test.com', offer)
      expect(ja.job_offer).to eq(offer)
    end
  end

  describe 'process' do
    it 'should deliver contact info notification' do
      ja = described_class.create_for('applicant@test.com', job_offer)
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_info_email, ja)
      ja.process
    end
  end

  describe 'format email' do
    it 'should be valid when complete applicant_email with applicant@test.com' do
      email = 'applicant@test.com'
      job_application = described_class.create_for(email, job_offer)
      expect(job_application.applicant_email).to eq(email)
    end

    [
      'appl@icant@test.com',
      'te@st@fi.uba.ar',
      'test@gmail..com',
      'te..st70@test.com',
      'test_70test.com'
    ].each do |email|
      it "should be invalid when complete applicant_email with #{email}" do
        check_validation(:applicant_email, 'Applicant email invalid format') do
          described_class.create_for(email, job_offer)
        end
      end
    end
  end
end
