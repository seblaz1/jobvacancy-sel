class JobOfferForm
  attr_accessor :id, :title, :location, :description, :expiration_date

  def self.from(a_job_offer)
    form = JobOfferForm.new
    form.id = a_job_offer.id
    form.title = a_job_offer.title
    form.expiration_date = a_job_offer.expiration_date
    form.location = a_job_offer.location
    form.description = a_job_offer.description
    form
  end
end
