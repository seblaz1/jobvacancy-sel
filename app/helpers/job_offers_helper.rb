# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  def job_offer_params
    params[:job_offer_form].to_h.symbolize_keys
  end

  def render_apply_form(offer_id)
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(offer_id))
    @job_application = JobApplicationForm.new
    # TODO: validate the current user is the owner of the offer
    render 'job_offers/apply'
  end

  def parse_time(date_string)
    expiration_date = Time.parse(date_string)
    Time.new(expiration_date.year, expiration_date.month, expiration_date.day,
             23, 59, 59)
  end
end
