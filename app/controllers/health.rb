if ENV['RACK_ENV'] == 'test' || ENV['RACK_ENV'] == 'development'
  JobVacancy::App.controllers :health do
    get :index do
      ping = Ping.create(description: 'health-controller')
      if ping.nil?
        status 500
      else
        status 200
        'ok'
      end
    end

    get :stats, provides: [:json] do
      { offers_count: JobOffer.count, users_count: User.count }.to_json
    end

    get :version do
      Version.current
    end

    get :reset do
      JobVacancy::App.settings.clock = Clock.new
      JobOfferRepository.new.delete_all
      UserRepository.new.delete_all
      'ok'
    end

    get :time do
      time = Time.parse(params['time'])
      JobVacancy::App.settings.clock.now = time
      'ok'
    end
  end
end
