require 'net/http'
require 'uri'

JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }
    begin
      @user = User.new(params[:user], UserRepository.new)

      if !captcha_api_verify(params['g-recaptcha-response'])
        flash.now[:error] = 'Captcha required'
        render 'users/new'
      elsif params[:user][:password] == password_confirmation
        create_new_user(@user)
      else
        flash.now[:error] = 'Passwords do not match'
        render 'users/new'
      end
    rescue PasswordInvalidException
      flash.now[:error] = 'Invalid password, it must be between 8 and 32 characters and ' \
                          'one special character (not alphanumeric)'
      render 'users/new'
    rescue DuplicateUserEmailException
      flash.now[:error] = 'User email already registered'
      render 'users/new'
    end
  end
end

def create_new_user(user)
  if UserRepository.new.save(user)
    flash[:success] = 'User created'
    redirect '/'
  else
    flash.now[:error] = 'All fields are mandatory'
    render 'users/new'
  end
end

def captcha_api_verify(recaptcha_response)
  return false if recaptcha_response.empty?

  res = Net::HTTP.post_form(
    URI.parse(JobVacancy::App.settings.recaptcha_api_url),
    'secret' => JobVacancy::App.settings.recaptcha_secret_key,
    'response' => recaptcha_response,
    'remoteip' => request.ip
  )

  JSON.parse(res.body)['success']
end
