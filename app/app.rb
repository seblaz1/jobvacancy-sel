module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    set :clock, Clock.new
    set :recaptcha_api_url, 'https://www.google.com/recaptcha/api/siteverify'

    Padrino.configure :development, :test do
      set :from_email, 'no_reply@jobvacancy.com'
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }
      set :recaptcha_site_key, '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
      set :recaptcha_secret_key, '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
    end

    Padrino.configure :staging, :production do
      set :from_email, ENV['FROM_EMAIL']
      set :delivery_method, smtp: {
        address: ENV['SMTP_ADDRESS'],
        port: ENV['SMTP_PORT'],
        user_name: ENV['SMTP_USER'],
        password: ENV['SMTP_PASS'],
        authentication: :plain,
        enable_starttls_auto: true
      }
      set :recaptcha_site_key, ENV['RECAPTCHA_SITE_KEY']
      set :recaptcha_secret_key, ENV['RECAPTCHA_SECRET_KEY']
    end
  end
end
